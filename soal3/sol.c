#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#define LOCATION "/home/bazokakaka/modul2/"

void _exec(char *cmd, char *argv[])
{
  pid_t child_id;
  int status;
  
  child_id = fork();

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0)
  {
    execv(cmd, argv);
  }

  while ((wait(&status)) > 0)
    ;
}

void change_dir(char *location)
{
  if ((chdir(location)) < 0)
  {
    exit(EXIT_FAILURE);
  }
}

void make_modul() {
  char *argv[] = {"mkdir", "/home/bazokakaka/modul2", NULL};
  _exec("/bin/mkdir", argv);
}

void make_dir()
{
  char first[100], second[100];
  strcpy(first, LOCATION);
  strcpy(second, LOCATION);
  strcat(first, "darat");
  strcat(second, "air");

  char *argv1[] = {"mkdir", first, NULL};
  _exec("/bin/mkdir", argv1);
  sleep(3);
  char *argv2[] = {"mkdir", second, NULL};
  _exec("/bin/mkdir", argv2);
}

void unzip()
{
  char *argv[] = {"unzip", "./animal.zip", "-d", LOCATION, NULL};
  _exec("/bin/unzip", argv);
}

int check_category(char *str) {
  if(strcmp(str, "darat") == 0) {
    return 1;
  } else if(strcmp(str, "air") == 0) {
    return 2;
  }
  return 0;
}

int check_bird(char *str) {
  int j = 0, k = 0;
  char strings[20][50];
  for (size_t i = 0; i < strlen(str); ++i)
  {
    if (str[i] == '_' || str[i] == '.')
    {
      strings[j][k] = '\0';
      k = 0;
      ++j;
      continue;
    }
    strings[j][k++] = str[i];
  }
  for (size_t i = 0; i < (size_t)j; ++i)
  {
    if(strcmp(strings[i], "bird") == 0) {
      return 0;
    }
  }
  return 1;
}

void move_file(char *str, int category) {
  if(!check_bird(str)) return;
  char location[100], str_final[100];
  strcpy(str_final, "animal/");
  strcat(str_final, str);
  if(category == 1) {
    strcpy(location, "darat");
    // printf("%s\n", location);
    
    char *argv[] = {"mv", str_final, location, NULL};
    _exec("/usr/bin/mv", argv);
  } else {
    strcpy(location, "air");
    // printf("%s\n", location);
    
    char *argv[] = {"mv", str_final, location, NULL};
    _exec("/usr/bin/mv", argv);
  }
  // printf("\n\n");
}

void insert_file(char *str)
{
  int j = 0, k = 0;
  char strings[20][50];
  for (size_t i = 0; i < strlen(str); ++i)
  {
    if (str[i] == '_' || str[i] == '.')
    {
      strings[j][k] = '\0';
      k = 0;
      ++j;
      continue;
    }
    strings[j][k++] = str[i];
  }
  for (size_t i = 0; i < (size_t)j; ++i)
  {
    int category = check_category(strings[i]);
    if(category) {
      move_file(str, category);
      // printf("%s: %s\n", strings[i], str);
      break;
    }
  }
}

void insert_files()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir("./animal");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (!(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0))
        continue;
      insert_file(ep->d_name);
    }

    (void)closedir(dp);
  }
  else
    perror("Can't open the directory");
}

void remove_unrelated_animals() {
  const *argv[] = {"rm", "-rf", "animal", NULL};
  _exec("/usr/bin/rm", argv);
}

int main() {
  make_modul();
  make_dir();
  unzip();
  change_dir(LOCATION);
  insert_files();
  remove_unrelated_animals();
  return 0;
}