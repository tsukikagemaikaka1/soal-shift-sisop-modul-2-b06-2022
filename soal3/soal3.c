#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

void Makedirectory();
void ExtracFile();

int main() {
  Makedirectory();
return 0;
}

void Makedirectory()
{
        pid_t child_id;
  int status;

  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child
    execl("/bin/mkdir","mkdir", "-p", "darat", NULL);
        sleep(3);
  }
else 
 {
    // this is parent
    while ((wait(&status)) > 0);
    execl("/bin/mkdir","mkdir", "-p", "air", NULL);
  }

}