#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#define LOCATION "/home/bazokakaka/shift2/drakor"

int status, cat_idx = 0;
char categories[20][100];

void _exec(char *cmd, char *argv[])
{
  pid_t child_id;
  child_id = fork();

  if (child_id < 0)
  {
    exit(EXIT_FAILURE);
  }

  if (child_id == 0)
  {
    execv(cmd, argv);
  }

  while ((wait(&status)) > 0)
    ;
}

void make_dir()
{
  char *argv1[] = {"mkdir", "/home/bazokakaka/shift2", NULL};
  _exec("/bin/mkdir", argv1);
  char *argv2[] = {"mkdir", "/home/bazokakaka/shift2/drakor", NULL};
  _exec("/bin/mkdir", argv2);
}

void unzip()
{
  char *argv[] = {"unzip", "./drakor.zip", "-d", LOCATION, NULL};
  _exec("/bin/unzip", argv);
}

void change_dir()
{
  if ((chdir(LOCATION)) < 0)
  {
    exit(EXIT_FAILURE);
  }
}

void remove_dir(char *dir_name)
{
  char *argv[] = {"remove", "-r", dir_name, NULL};
  _exec("/usr/bin/rm", argv);
}

int check_dir(char *str)
{
  for (size_t i = 0; i < strlen(str); ++i)
  {
    if (str[i] == '.')
      return 0;
  }
  return 1;
}

void remove_directory()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir(".");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (!(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0))
        continue;
      if (check_dir(ep->d_name))
      {
        remove_dir(ep->d_name);
      }
    }

    (void)closedir(dp);
  }
  else
    perror("Can't open the directory");
}

int check_year(char *str)
{
  if (str[0] == '2')
    return 1;
  return 0;
}

void remove_underscore(char *str)
{
  for (int i = 0; i < strlen(str); ++i)
  {
    if (str[i] == '_')
    {
      str[i] = '\0';
      return 0;
    }
  }
}

int is_unique(char *str)
{
  for (int i = 0; i <= cat_idx; ++i)
  {
    if (strcmp(categories[i], str) == 0)
      return 0;
  }
  return 1;
}

void get_category(char *str)
{
  int j = 0, k = 0;
  char strings[20][50];
  char category[10][50];
  for (size_t i = 0; i < strlen(str); ++i)
  {
    if (str[i] == ';' || str[i] == '.')
    {
      strings[j][k] = '\0';
      k = 0;
      ++j;
      continue;
    }
    strings[j][k++] = str[i];
  }
  for (size_t i = 0; i < (size_t)j; ++i)
  {
    if (check_year(strings[i]))
    {
      remove_underscore(strings[++i]);
      if (is_unique(strings[i]))
        strcpy(categories[cat_idx++], strings[i]);
    }
  }
}

void create_folder(char *str)
{
  char *argv[] = {"mkdir", "-p", str, NULL};
  _exec("/bin/mkdir", argv);
}

void initialize_data_file(char *str) {
  char str1[100];
  strcpy(str1, str);
  strcat(str1, "/data.txt");
  printf("%s\n\n", str1);
  char *argv[] = {"touch", str1, NULL};
  _exec("/usr/bin/touch", str1);
}

void create_data_file(char *str) {
  char text[100];

  strcpy(text, "kategori: ");
  strcat(text, str);

  char file_location[100];
  strcpy(file_location, "./");
  strcat(file_location, str);
  strcat(file_location, "/data.txt");

  printf("%s\n%s\n\n", text, file_location);
  char *argv[] = {"touch", file_location};
  _exec("/usr/bin/touch", argv);
  // char *argv[] = {"echo", text, ">>", file_location, NULL};
  // _exec("echo", argv);
}

void create_category_folders()
{
  for (int i = 0; i < cat_idx; ++i)
  {
    create_folder(categories[i]);
    create_data_file(categories[i]);
  }
}

void create_categories()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir(".");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (!(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0))
        continue;
      get_category(ep->d_name);
    }

    (void)closedir(dp);
  }
  else
    perror("Can't open the directory");
  create_category_folders();
}

void move_file(char *category, char *file_name)
{
  char *argv[] = {"cp",
                  file_name,
                  category,
                  NULL};
  _exec("/bin/cp", argv);
}

void add_data_file(char *category, char *year, char *file_name) {
  char data_file_name[100];
  strcat(data_file_name, "./");
  strcat(data_file_name, category);
  strcat(data_file_name, "/data.txt");


  // printf("%s\n\n", data_file_name);

  char file_content[1000] = "";

  int i = 0;
  while(file_name[i] != '.') ++i;
  char title[100];
  strcpy(title, file_name);
  title[i] = '\0';

  char *argv[] = {"echo", "\ntest\n", ">>", data_file_name};

  strcpy(file_content, "\n");
  strcpy(file_content, "nama:");
  strcat(file_content, title);
  strcat(file_content, "\n");
  strcat(file_content, "rilis :tahun ");
  strcat(file_content, year);
  strcat(file_content, "\n");

  // printf("%s\n\n", file_content);  
  // _exec("/bin/echo", argv);
}

char *rename_file(char *file_name, char *title) {
  strcat(title, ".png");
  char *argv[] = {"mv", file_name, title, NULL};
  _exec("/bin/mv", argv);
  return title;
}

void insert_file_to_category(char *category, char *file_name, char *title, char *year)
{
  // printf("%s\n", category);
  for (int i = 0; i <= cat_idx; ++i)
  {
    if (strcmp(category, categories[i]) == 0)
    {
      // printf("%s\n", category);
      strcpy(file_name, rename_file(file_name, title));
      add_data_file(category, year, file_name);
      move_file(category, file_name);
    }
  }
}

void insert_file(char *str)
{
  int j = 0, k = 0, a = 0, b = 0;
  char strings[20][50];
  char category[10][50];
  for (size_t i = 0; i < strlen(str); ++i)
  {
    if (str[i] == ';' || str[i] == '.' || str[i] == '_')
    {
      strings[j][k] = '\0';
      k = 0;
      ++j;
      continue;
    }
    strings[j][k++] = str[i];
  }
  for (size_t i = 0; i < (size_t)j; ++i)
  {
    if (check_year(strings[i])) {
      insert_file_to_category(strings[i + 1], str, strings[i - 1], strings[i]);
      ++i;
    }
  }
}

void insert_files()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir(".");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (!(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0))
        continue;
      insert_file(ep->d_name);
    }

    (void)closedir(dp);
  }
  else
    perror("Can't open the directory");
}

int check_file(char *str)
{
  for (size_t i = 0; i < strlen(str); ++i)
  {
    if (str[i] == '.')
      return 1;
  }
  return 0;
}

void remove_files()
{
  DIR *dp;
  struct dirent *ep;

  dp = opendir(".");

  if (dp != NULL)
  {
    while ((ep = readdir(dp)))
    {
      if (!(strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0))
        continue;
      if (check_file(ep->d_name))
      {
        remove_dir(ep->d_name);
      }
    }

    (void)closedir(dp);
  }
  else
    perror("Can't open the directory");
}

int main()
{
  make_dir();
  unzip();
  change_dir();
  remove_directory();
  create_categories();
  insert_files();
  remove_files();
  return 0;
}