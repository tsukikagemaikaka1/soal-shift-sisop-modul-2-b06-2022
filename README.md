# Praktikum Modul 2 B06

Anggota kelompok:
<br>

- Hesekiel Nainggolan (5025201054)
- Rycahaya Sri Hutomo (5025201046)
- Yehezkiel Wiradhika (5025201086)

# **soal-shift-sisop-modul-2-B06-2022**

## <strong>Nomor 1</strong>

<br><strong>PENJELASAN SOAL</strong><br>
<strong>a.<br></strong>

- _download_ file _characters_ dan file _weapons_ dari _link_
  > Database item characters :
  > https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view > <br>Database item weapons :
  > https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view
- mengekstrak kedua file tersebut.
- membuat folder sebagai _working directory_, format nama: <code>gacha_gacha</code>.

**b.** pada setiap kali gacha,<br>

- mengambil data item characters dan item weapons secara bergantian dari database.
- melakukan gacha item weapons jika jumlah gacha genap.
- melakukan gacha item characters jika jumlah gacha ganjil.
- membuat file baru (<code>.txt</code>) dan meletakkan output hasil gacha di file baru tersebut, jika jumlah gacha mod 10.
- membuat folder baru dan file <code>.txt</code> selanjutnya diletakkan di folder tersebut, jika jumlah gacha mod 90.
  > Setiap folder terdapat 9 file (<code>.txt</code>) yang di dalamnya berisi 10 hasil gacha.<br>
  > Hasil gacha di file (<code>.txt</code>) adalah **ACAK/RANDOM** dan setiap file (<code>.txt</code>) isinya akan **BERBEDA**.

**c.** Format nama:

- setiap file (<code>.txt</code>) : <code>{Hh:Mm:Ss}\_gacha\_{jumlah-gacha}</code>.
- setiap folder: <code>total_gacha\_{jumlah-gacha}</code>.
- setiap file (<code>.txt</code>) memiliki perbedaan penamaan waktu output sebesar **1 second**.

**d.**

- _define_ awal primogems (alat tukar untuk melakukan gacha item) sebanyak **79000**.
- setiap kali gacha, mengambil **2 properties** dari database, yaitu **name** dan **rarity**.
- output hasil gacha ke file (<code>.txt</code>) dengan format <code>{jumlah-gacha}\_[tipe-item]\_{rarity}\_{name}\_{sisa-primogems}</code>.

**e.**

- proses melakukan gacha pada 30 Maret jam 04.44 (anniversary pertama kali bermain bengshin impek).
- 3 jam setelah anniversary (pukul 07.44),zip semua isi folder <code>gacha_gacha</code> dengan nama <code>not_safe_for_wibu</code> dan password dengan <code>satuduatiga</code>.
- hapus semua folder dan sisakan file <code>.zip</code>.

<br>**PENJELASAN JAWABAN**<br>
<br>

## <strong>Nomor 2</strong>

<br><strong>PENJELASAN SOAL</strong><br>
<strong>a.<br></strong>

- mengekstrak zip ke folder <code>“/home/[user]/shift2/drakor</code>.
- membedakan file dan folder.
- memproses file yang seharusnya dikerjakan (bentuk <code>.png</code>).
- menghapus folder yang tidak dibutuhkan.

<strong>b.</strong><br>

- membuat folder drama korea yang ada di dalam zip sesuai jenis.

<strong>c.</strong><br>

- memindahkan poster ke folder sesuai kategori.
- rename dengan nama <code>/drakor/romance/start-up.png</code>.

<strong>d.</strong><br>

- Foto yang terdapat di lebih dari satu poster dipindah ke folder sesuai masing-masing kategori yang sesuai. Contoh: foto dengan nama <code>start-up;2020;romance_the-k2;2016;action.png</code> dipindah ke folder <code>/drakor/romance/start-up.png</code> dan <code>/drakor/action/the-k2.png</code>.

<strong>e.</strong><br>

- Di setiap kategori folder, membuat file <code>data.txt</code> yang berisi nama dan tahun rilis semua drama korea di folder tersebut.
- Sorting list di file berdasarkan tahun rilis (_ascending_). Format :

> kategori : romance
>
> nama : start-up<br>
> rilis : tahun 2020<br>
>
> nama : twenty-one-twenty-five<br>
> rilis : tahun 2022

<br>**PENJELASAN JAWABAN**<br>

- model cara pengerjaan yang utama adalah dengan membuat fungsi \_exec() yang akan membuat child process saat kita melakukan command linux melalui program c

  ```
  void _exec(char *cmd, char *argv[])
  {
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0)
    {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0)
    {
      execv(cmd, argv);
    }

    while ((wait(&status)) > 0)
      ;
  }
  ```

- terdapat tujuh fungsi utama sebagai berikut:
  1. <strong>make_dir()</strong>
     <br>
     merupakan fungsi untuk membuat folder shift2/drakor pada LOCATION
     <br><br>
  2. <strong>unzip()</strong>
     <br>
     merupakan fungsi untuk mengekstrak file ke LOCATION
     <br><br>
  3. <strong>change_dir()</strong>
     <br>
     merupakan fungsi untuk mengganti posisi relatif program ke LOCATION
     <br><br>
  4. <strong>remove_directory()</strong>
     <br>
     merupakan fungsi untuk melakukan delete pada semua directory dalam LOCATION sehingga hanya terdapat file yang digunakan dalam soal
     <br><br>
  5. <strong>create_categories()</strong>
     <br>
     merupakan fungsi untuk membuat kategori-kategori folder yang sesuai dengan kategori file
     <br><br>
  6. <strong>insert_files()</strong>
     <br>
     merupakan fungsi untuk memasukkan file-file sesuai dengan kategorinya masing-masing
     <br><br>
  7. <strong>remove_files()</strong>
     <br>
     merupakan fungsi untuk meremove file-file yang tidak terdapat dalam kategori (karena program melakukan copying file ke masing-masing kategori sehingga kita perlu melakukan delete pada file-file yang lama)
     <br><br>

<br>**SCREENSHOOT PROGRAM**<br>
[hasil folder](https://drive.google.com/file/d/18l_cM8qhhhmA5iJhQj34F_qtGX5lWEH7/view?usp=sharing)
<br>
[hasil file](https://drive.google.com/file/d/1Olbbi6FYPeVFlmG4bgwk7JEm6XxkjJ0c/view?usp=sharing)

## <strong>Nomor 3</strong>

<br><strong>PENJELASAN SOAL</strong><br>
Pada soal nomor 3, kita diminta untuk membuat program laporan klasifikasi hewan-hewan yang hilang, adapun isi dari laporan ialah :

**a.** Kita diminta untuk membuat 2 directory, yaitu directory <code>darat</code> dan <code>air</code>.<br>
**b.** Kita diminta untuk membuat program ekstrak file <code>animal.zip</code> pada directory <code>/home/user/modul2</code>.<br>
**c.** Hasil dari file yang diekstrak akan dimasukkan ke dalam directory masing-masing sesuai dengan jenis hewan, yaitu directory <code>/home/user/modul2/darat</code> untuk hewan jenis darat dan <code>/home/user/modul2/air</code> untuk hewan jenis air. Jika ada hewan yang tidak memiliki keterangan darat maupun air maka akan dihapus.<br>
**d.** Setelah berhasil memisahkan antara hewan darat dan air, kita diminta untuk semua burung yang terdapat pada directory <code>/home/user/modul2/darat</code>, dengan keterangan file burung memiliki **“bird”** pada nama file.<br>
Setelah itu kita diminta untuk membuat file <code>.txt</code> dimana menampung semua nama-nama hewan pada directory <code>/home/user/modul2/air</code> dengan penamaan <code>UID\_[UID file permission]\_Nama File.[jpg/png]</code>.<br>

## Penjelasan Jawaban

- model cara pengerjaan yang utama adalah dengan membuat fungsi \_exec() yang akan membuat child process saat kita melakukan command linux melalui program c

  ```
  void _exec(char *cmd, char *argv[])
  {
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0)
    {
      exit(EXIT_FAILURE);
    }

    if (child_id == 0)
    {
      execv(cmd, argv);
    }

    while ((wait(&status)) > 0)
      ;
  }
  ```

- terdapat enam fungsi utama sebagai berikut:
  1. <strong>make_modul()</strong>
     <br>
     merupakan fungsi untuk membuat folder modul2 sesuai permintaan soal
     <br><br>
  2. <strong>make_dir()</strong>
     <br>
     merupakan fungsi untuk membuat folder darat dan air pada folder modul2
     <br><br>
  3. <strong>unzip()</strong>
     <br>
     merupakan fungsi untuk mengekstrak file zip pada soal
     <br><br>
  4. <strong>change_dir(LOCATION)</strong>
     <br>
     merupakan fungsi untuk merubah posisi relatif program c yang sedang berjalan ke folder modul2
     <br><br>
  5. <strong>insert_files()</strong>
     <br>
     merupakan fungsi yang kita gunakan untuk menaruh file-file pada folder yang sesuai
     <br><br>
  6. <strong>remove_unrelated_animals()</strong>
     <br>
     merupakan fungsi untuk meremove file-file yang diminta dalam soal

<br>**SCREENSHOOT PROGRAM**<br>
[hasil folder](https://drive.google.com/file/d/1eP3DxbjqouXC1ncAZQcDoBuWtaJdsvRn/view?usp=sharing)
<br>
[hasil file folder air](https://drive.google.com/file/d/12d4v-bwpSiYUyTK6DZ1uIoe4lP9Ax4sN/view?usp=sharing)
<br>
[hasil file folder darat](https://drive.google.com/file/d/1NVX82kAARkz0d-Vrgyr1OtFqDD26I77U/view?usp=sharing)
